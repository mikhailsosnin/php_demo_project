<?php

namespace EPAM\training\PHP\Sample\Calculator;

class CLPProcessor
{
    private array $clpArray;
    private bool $isClpValid;

    public function __construct(array $clpArray)
    {
        $this->clpArray = $clpArray;
        $this->isClpValid = $this->testClp();
    }

    private function testClp(): bool
    {
        if ((sizeof($this->clpArray) >= 4) &&
            (is_numeric($this->clpArray[1])) &&
            (is_numeric($this->clpArray[3])) &&
            (strpos("+-*/", $this->clpArray[2])) !== false) {
            return true;
        } else {
            return false;
        }
    }

    public function getFirstOperand(): float
    {
        if (!$this->isClpValid) {
            throw new \Exception("Can't get the first operand on invalid command line parameters set");
        } else {
            return (float)$this->clpArray[1];
        }
    }

    public function getSecondOperand(): float
    {
        if (!$this->isClpValid) {
            throw new \Exception("Can't get the second operand on invalid command line parameters set");
        } else {
            return (float)$this->clpArray[3];
        }
    }

    public function getOperation(): string
    {
        if (!$this->isClpValid) {
            throw new \Exception("Can't get the operation on invalid command line parameters set");
        } else {
            return $this->clpArray[2];
        }
    }
}
