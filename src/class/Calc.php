<?php

namespace EPAM\training\PHP\Sample\Calculator;

class Calc
{
    private int $precision;

    public function __construct(int $precision)
    {
        $this->precision = $precision;
    }

    public function sum(int|float $a, float $b) : int|float
    {
        return round($a + $b, $this->precision);
    }

    public function sub(int|float $a, float $b) : int|float
    {
        return round($a - $b, $this->precision);
    }

    public function mul(int|float $a, float $b) : int|float
    {
        return round($a * $b, $this->precision);
    }

    public function div(int|float $a, float $b) : int|float
    {
        return round($a / $b, $this->precision);
    }

}